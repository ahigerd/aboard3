## ABoard 3.0 for Google App Engine

ABoard 3.0 is intended to be a lightweight, high-scalability web forum.

Its primary design goal is to be inexpensive for small sites to operate
without the sluggishness and unreliability of most cheap web hosts, while
being scalable enough that large sites stay fast and responsive even
in the face of heavy traffic.

Its secondary design goal is to be fully documented with clean code to make
per-site customizations and additions easy for third-party developers to
create.

Notable non-goals include:

* Loadable plugins
* Database-driven theming
* Tons of built-in features
* View counts / viewer status

These are common features of other web forum systems, such as phpBB and
vBulletin, but they conflict with ABoard's design goals. ABoard's design
philosophy says that code should be code, data should be data, and both
should be lean and elegant for efficiency and maintainability. 

Loadable plugins and database-driven theming put code in the database and
impose limitations on their flexibility by the necessity of that structure.
ABoard considers this anathema to its ideals. Instead, ABoard's clean design
is intended to make theming simple in CSS and extension features
straightforward to add directly to the code.

Lots of built-in features also runs counter to the idea of having a lean,
high-performance core. Why should websites have to pay a performance cost
for features they don't care about? Hopefully, any desirable features should
be easily added for sites that want them.

View counts and viewer status ("Who's Online", "Currently viewing this
thread", etc.) are common features in other web forums, but such features
are surprisingly expensive on a platform like Google App Engine, where sites
are billed on the number of writes to the database. 

## Licensing
Copyright 2016 Adam Higerd

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

   http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

The full text of this license can also be found in the [LICENSE](LICENSE)
file.

## Authors
Adam Higerd <chighland@gmail.com>
