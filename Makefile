lint:
	pylint *.py aboard/*.py model/*.py

pip:
	pip install -U -r requirements.txt -t lib/

pip27:
	pip-2.7 install -U -r requirements.txt -t lib/

clean:
	@rm -f *.pyc aboard/*.pyc model/*.pyc templates/*.pyc

deploy:
	appcfg.py -A `grep application: app.yaml | sed "s/application: *//"` -V `git rev-parse --abbrev-ref HEAD` update ./
