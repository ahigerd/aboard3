"""General-purpose or generic utility functions used in various places
in the ABoard code.
"""

import base64
import binascii
import functools
import inspect
import math
import sys
import traceback
import os
import re

import bottle
import jinja2


IS_DEV_SERVER = os.environ.get('APPLICATION_ID', '').startswith('dev~')
HIDE_TRACEBACKS = not IS_DEV_SERVER


class _RenderOutput(object):
  """A thin wrapper class around a template with arguments, for the purposes
  of accessing the macros defined in the template.
  """
  def __init__(self, filename, **kwargs):
    """Load a template from the specified filename and populate the object
    with the parameters to be used in rendering it.
    """
    self.template = template_env.get_template(filename)
    self.kwargs = kwargs.copy()

  def render(self):
    """Render the template with the stored arguments."""
    return self.template.render(self.kwargs)

  def macro(self, name, *args, **kwargs):
    """Execute a macro from the template, if it exists, passing it the
    stored arguments in addition to any passed to the macro() call.
    """
    fn = getattr(self.template.make_module(self.kwargs), name, None)
    if not fn:
      return None
    combined = self.kwargs.copy()
    combined.update(kwargs)
    # Strip out unused kwargs if the function doesn't accept arbitrary kwargs
    if not fn.catch_kwargs:
      combined = dict((k, v) for k, v in combined.items() if k in fn.arguments)
    # Truncate the argument list if the function doesn't accept varargs
    if not fn.catch_varargs:
      args = args[:len(fn.arguments)]
    return fn(*args, **combined)


def render(name, **kwargs):
  """Load a template and render it using the provided values."""
  if 'session' not in globals():
    # must lazy-import here to avoid circular dependencies
    # avoid importing App Engine in the template rebuilder
    global session
    import session

  return _RenderOutput(name + ".html", session=session.current_session(), **kwargs)


def render_page(body, **kwargs):
  """Load and render the main page template, filling it with the provided
  content. The keyword arguments are unused by the default implementation
  but are passed to the main page template for third-party extensibility.
  """
  title = False
  if isinstance(body, _RenderOutput):
    content = body.render()
    title_macro = body.macro('title')
    if title_macro:
      title = title_macro
  else:
    content = body
  return render("page", content=content, title=title, **kwargs).render()


def long_to_urlkey(long_int, friendly_prefix=''):
  """Convert a Python long to a urlkey.

  urlkeys are a lossless way to output large numeric values with a compact,
  less-ugly representation that is opaque to the user. A human-readable
  prefix can be supplied that will be prepended to the urlkey and ignored
  during decoding.
  """
  if friendly_prefix:
    friendly_prefix = re.sub(r"[^a-z0-9]+", '-', friendly_prefix.lower() + '-')
  hexrep = bytes('{:x}').format(long_int)
  if len(hexrep) % 1:
    hexrep = '0' + hexrep
  return friendly_prefix + base64.b64encode(
      binascii.rlecode_hqx(bytearray.fromhex(hexrep)),
      altchars="._").replace('=', '')


_padding = ['', '', '==', '=']
def urlkey_to_long(key):
  """Convert a urlkey-encoded value to a Python long.

  If the urlkey has a friendly prefix, it is ignored. If the urlkey is
  malformed, returns None instead of throwing an exception.
  """
  try:
    key = key.rpartition('-')[2]
    key += _padding[len(key) % 4]
    return long(binascii.hexlify(binascii.rledecode_hqx(base64.b64decode(key, altchars="._"))), 16)
  except TypeError:
    return None


def urlkeys(title_field):
  """Decorator. Make the decorated class offer the default urlkey functions:
  the urlkey instance property, the decode_urlkey class method, and the
  urlkey_get class method. A class may override the default functionality by
  providing any of these functions in its definition.

  If title_field is not None, use it as the name of a field in the model to
  use for the friendly urlkey prefix.
  """
  if 'ndb' not in globals():
    # must lazy-import here to avoid importing App Engine in the template rebuilder
    global ndb
    from google.appengine.ext import ndb
  @property
  def urlkey(self, use_prefix=True):
    """Generate a urlkey for this entity.

    If use_prefix is True, and if the forum's title is populated and
    non-empty, then the title will be used for a friendly prefix.
    """
    try:
      if use_prefix and title_field and getattr(self, title_field, False):
        return long_to_urlkey(self.key.id(), getattr(self, title_field))
    except ndb.UnprojectedPropertyError:
      pass
    return long_to_urlkey(self.key.id())

  @classmethod
  def decode_urlkey(cls, urlkey):
    """Transform a urlkey into an ndb.Key object of this kind."""
    key = ndb.Key(cls, urlkey_to_long(urlkey))
    if key.id() is None:
      return None
    return key

  @classmethod
  def urlkey_get(cls, urlkey):
    """Retrieve an entity based on a urlkey."""
    key = cls.decode_urlkey(urlkey)
    if not key:
      return None
    return key.get()

  def add_methods(cls):
    """Add the urlkey methods to the provided class, unless the class has
    provided its own implementations.
    """
    if not getattr(cls, 'urlkey', None):
      cls.urlkey = urlkey
    if not getattr(cls, 'decode_urlkey', None):
      cls.decode_urlkey = decode_urlkey
    if not getattr(cls, 'urlkey_get', None):
      cls.urlkey_get = urlkey_get
    return cls
  return add_methods


def install_filters(env): # pylint: disable=unused-argument
  """Install custom filters into the Bottle environment."""
  pass # there currently aren't any custom filters


template_env = jinja2.Environment(loader=jinja2.ModuleLoader("templates"),
                                  trim_blocks=True,
                                  auto_reload=IS_DEV_SERVER)
install_filters(template_env)


def redirect_to(url, absolute=False):
  """Redirect the user's browser to the specified URL.

  Return the empty string as a convenience to enable
      return redirect_to('/foo')
  as an idiom.

  If absolute is set to True, the specified URL is interpreted relative to
  the server root. If absolute is set to False (the default), the specified
  URL is interpreted relative to the app handling the current request.
  """
  bottle.response.status = 301
  if absolute:
    bottle.response.set_header('Location', url)
  else:
    newurl = bottle.request.script_name + url
    newurl = newurl.replace('//', '/')
    bottle.response.set_header('Location', newurl)
  return ''


def show_exceptions(fn):
  """Decorator. Make the decorated function render tracebacks in HTML if
  it throws an exception.

  This behavior can be disabled by setting aboard_util.HIDE_TRACEBACKS
  to True, which is the default when deployed to Google App Engine.
  """
  argspec = inspect.getargspec(fn)
  env = {'fn': fn, '_G': globals(), 'Exception': Exception, 'traceback': traceback}
  exec """def with_shown_exceptions""" + inspect.formatargspec(*argspec) + """:
    try:
      return fn""" + inspect.formatargspec(*argspec[0:3]) + """
    except Exception, e:
      if _G['HIDE_TRACEBACKS']:
        raise
      return '<pre>' + traceback.format_exc(e).replace('<', '&lt;') + '</pre>'
    return with_shown_exceptions""" in env
  return functools.update_wrapper(env['with_shown_exceptions'], fn)


@show_exceptions
def redirect_index():
  """Redirect a request to a URL with a trailing slash appended."""
  url = bottle.request.urlparts.path + '/'
  return redirect_to(url)


def make_mount_at(app):
  """Return a function that will, given a target app and a prefix, mount
  the given app into the target app at the specified prefix.

  As a technical requirement for generated documentation, the given app
  must be stored in a module-level variable. If the app object cannot be
  found, the mount_at documentation will not appear.
  """
  def mount_at(root_app, prefix):
    """Mount this app into root_app at the path named by prefix.

    In addition, redirect requests to the prefix without a trailing
    slash to the prefix with a trailing slash.
    """
    if prefix.endswith('/'):
      withslash = prefix
      withoutslash = prefix[:-1]
    else:
      withslash = prefix + '/'
      withoutslash = prefix

    root_app.mount(withslash, app)
    root_app.add_route(bottle.Route(app, withoutslash, 'GET', redirect_index))

  # We can't use inspect.getmodule because it doesn't work inside the App Engine environment.
  # (It expects sys.modules['__main__'] to exist, but it doesn't.)
  for mod in sys.modules.keys():
    for member in dir(sys.modules[mod]):
      if getattr(sys.modules[mod], member) is app:
        mount_at.__module__ = mod
        return mount_at
  return mount_at


#: A list of replacements, in the form:
#  [pattern, substitution, works when bbcode disabled]
_replacements = [
    [r'&', r'&amp;', True],
    [r"'", r'&#39;', True],
    [r'"', r'&quot;', True],
    [r'<', r'&lt;', True],
    [r'>', r'&gt;', True],
    ['\n', r'<br/>', True],
    [r'\[([bius]|strike)](.*)\[/\1]', r'<\1>\2</\1>', False],
    [r'\[\[]', r'[', False],
    [r'\[\]]', r']', False],
]
for code in _replacements:
  code[0] = re.compile(code[0], re.I | re.M | re.S)


def format_text(text, use_bbcode):
  """Apply HTML formatting to a user-provided string, optionally converting
  bbcode formatting into HTML.
  """
  # Ensure that the incoming string is considered Unicode
  if not isinstance(text, unicode):
    text = text.decode('utf-8')
    assert isinstance(text, unicode)
  for rep in _replacements:
    if use_bbcode or rep[2]:
      text = rep[0].sub(rep[1], text)
  return jinja2.Markup(text)


class SortControls(object):
  """Provides a collection of sort ascending/descending arrows for a table
  of data."""
  def __init__(self, baseurl, current, reverse=False):
    """Construct the sort controls linking to a given base URL, with current
    and reverse specifying the currently-selected sort order.
    """
    self.template = template_env.get_template("sortarrows.html")
    self.baseurl = baseurl
    self.current = current
    self.reverse = reverse

  def __getattr__(self, name):
    """Generate HTML for the sort arrows for the named column."""
    is_current = (self.current == name)
    return self.template.render(
        uplink=self.baseurl + '?sort=' + name + '-a',
        downlink=self.baseurl + '?sort=' + name + '-d',
        up=is_current and not self.reverse,
        down=is_current and self.reverse)


class PageControls(object):
  """Provides a set of page controls and the associated math functions for
  a paged set of data.
  """
  def __init__(self, entrycount, perpage, current, querystring=''):
    """Construct page controls for a list with entrycount entries and
    perpage entries per page, which is currently viewing page current.

    If a query string is provided, append it to each page link.
    """
    self.template = template_env.get_template("pagecontrols.html")
    self.pagecount = int(math.ceil(entrycount / (1.0 * perpage)))
    if current is None:
      self.current = 1
    else:
      self.current = int(current)
      if self.current < 1:
        self.current = 1
    self.pagestart = (self.current - 1) * perpage
    if querystring.startswith('?'):
      self.querystring = querystring
    else:
      self.querystring = '?' + querystring

  def render(self):
    """Generate HTML for the pager controls."""
    if self.pagecount < 13:
      pagelist = range(1, self.pagecount+1)
    elif self.current < 7:
      pagelist = (range(1, max(4, self.current+2)) +
                  [True] +
                  range(self.pagecount-2, self.pagecount+1))
    elif self.current > self.pagecount-6:
      pagelist = [1, 2, 3, True] + range(self.current-1, self.pagecount+1)
    else:
      pagelist = [1, 2, 3, True,
                  self.current-1, self.current, self.current+1,
                  True] + range(self.pagecount-2, self.pagecount+1)
    return self.template.render(
        querystring=self.querystring,
        pagelist=pagelist,
        current=self.current)

  def __str__(self):
    """Alias for self.render()."""
    return self.render()
