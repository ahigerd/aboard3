"""Session management functionality.

This module contains a single Bottle method which may be installed in any
Bottle app to provide login-related pages for the site.

Some further investigation is necessary to identify the ideal way of
managing the cache.
"""

import datetime
import re

from aboard_util import *
from google.appengine.api import memcache
from google.appengine.api import runtime
from google.appengine.ext import ndb
import model


def _ensure_in_session_list(sid):
  """Make sure the session ID is in the master list of memcached sessions."""
  sessions = memcache.get("sessionlist")
  if not sessions:
    sessions = set()
  sessions.add(sid)
  memcache.set("sessionlist", sessions)


def _remove_from_session_list(sid):
  """Make sure the session ID has been removed from the master list of
  memcached sessions.
  """
  sessions = memcache.get("sessionlist")
  if not sessions or sid not in sessions:
    return
  sessions.remove(sid)
  memcache.set("sessionlist", sessions)


def current_session():
  """Return the session for the current request, creating a new session if
  one does not already exist.
  """
  session = None
  sid = bottle.request.get_cookie('sid')
  now = datetime.datetime.now()
  if not sid:
    # There's no session ID, so this is the first request from a browser
    session = model.Session()
    sid = session.put().urlsafe()
    memcache.set(sid, session, namespace="session")
  else:
    # There's a session ID, so restore it
    session = memcache.get(sid, namespace="session")
    if session:
      # The session was found in the cache
      if session.user:
        session.user.prefetch()
      if session.last_update < now - datetime.timedelta(minutes=1):
        # Update it if it hasn't been done recently
        session.last_update = datetime.datetime.now()
        memcache.set(sid, session, namespace="session")
    else:
      # The session wasn't in the cache, load it from the database
      session = ndb.Key(urlsafe=sid).get()
      if not session:
        # The session wasn't in the database, either; make a new one
        session = model.Session()
        sid = session.put().urlsafe()
      else:
        if session.user:
          session.user.prefetch()
        session.last_update = datetime.datetime.now()
      memcache.set(sid, session, namespace="session")
  _ensure_in_session_list(sid)
  bottle.response.set_cookie('sid', sid, max_age=86400*7)
  write_sessions(True)
  return session


def write_sessions(idle_only=False):
  """Write all of the outstanding memcached sessions to persistent storage.

  If idle_only is True, only write sessions that have not been updated in
  the last hour.
  """
  threshold = datetime.datetime.now()
  if idle_only:
    threshold = threshold - datetime.timedelta(hours=1)
  sessions = memcache.get("sessionlist")
  if not sessions:
    return
  for sid in sessions:
    session = memcache.get(sid, "session")
    if getattr(session, 'dirty', False) and session.last_update < threshold:
      session.put()


def _login_user(user, redirect_url='/', absolute=True):
  session = current_session()
  session.user = user.key
  session.put()
  memcache.set(session.key.urlsafe(), session, namespace='session')
  return redirect_to(redirect_url, absolute=absolute) # TODO: return to origin page


@show_exceptions
def login_page():
  """Handle the login page.

  If the current session is already logged in, redirect to the logout page.
  """
  session = current_session()
  if session.user is not None:
    return redirect_to("/logout")
  username = ''
  error = False
  if 'login' in bottle.request.POST:
    username = bottle.request.POST['username']
    user = model.User.query(model.User.username == username).get()
    if user and user.check_password(bottle.request.POST['password']):
      return _login_user(user)
    else:
      error = True
  return render_page(
      render("login", username=username, error=error))


@show_exceptions
def logout_page():
  """Handle the logout page.

  If the current session is already logged out, redirect to the login page.
  """
  sid = bottle.request.get_cookie('sid')
  if not sid or current_session().user is None:
    return redirect_to('/login')
  if 'logout' in bottle.request.POST:
    if 'everywhere' in bottle.request.POST:
      # Query all of the sessions for the user out of the database
      query = model.Session.query(model.Session.user == current_session().user)
      for session in query.iter(keys_only=True):
        # Delete the session and make sure no one can re-establish it for a while
        # Do this in a retry loop because this is important to avoid session takeover
        session_sid = session.urlsafe()
        while not memcache.delete(session_sid, namespace="session", seconds=86400):
          pass
        session.delete()
      memcache.set("sessionlist", set())
    else:
      # Delete the session and make sure no one can re-establish it for a while
      # Do this in a retry loop because this is important to avoid session takeover
      ndb.Key(urlsafe=sid).delete()
      _remove_from_session_list(sid)
      while not memcache.delete(sid, namespace="session", seconds=86400):
        pass
    bottle.response.delete_cookie('sid')
    return redirect_to("/") # TODO: return to previous page
  return render_page(render("logout"))


@show_exceptions
def register_page():
  """Handle the registration page."""
  missing_fields = False
  name_in_use = False
  invalid_email = False
  password_mismatch = False
  if 'register' in bottle.request.POST:
    username = bottle.request.POST['username'].strip()
    email = bottle.request.POST['email'].strip()
    password = bottle.request.POST['password'].strip()
    confirm = bottle.request.POST['confirm'].strip()
    password_mismatch = password != confirm
    missing_fields = not username or not email or not password or not confirm
    if username:
      if model.User.query(model.User.username == username).count(limit=1):
        name_in_use = True
    if not re.match(r"^[^@]+@[^@.]+\.[^@]{3,}$", email):
      invalid_email = True
    if not missing_fields and not name_in_use and not invalid_email and not password_mismatch:
      user = model.User(username=username, email=email)
      current_session().user = user.put()
      user.set_password(password)
      user.put()
      return _login_user(user)
  else:
    username = ''
    email = ''
  return render_page(
      render("register",
             username=username,
             email=email,
             missing_fields=missing_fields,
             name_in_use=name_in_use,
             password_mismatch=password_mismatch,
             invalid_email=invalid_email))


def mount_in(app):
  """Add a route for the login and registration pages to the provided app.
  """
  app.route("/login", method=('GET', 'POST'))(login_page)
  app.route("/logout", method=('GET', 'POST'))(logout_page)
  app.route("/register", method=('GET', 'POST'))(register_page)


runtime.set_shutdown_hook(write_sessions)
