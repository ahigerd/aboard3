"""This module contains a Bottle app that will render Python source code as
syntax-highlighted HTML pages for any requested module it can import.
"""

import os

from aboard_util import *
import bottle


app = bottle.Bottle()


@app.route('/')
@show_exceptions
def render_index():
  """Render a list of files to browse."""
  mods = []
  for f in os.listdir('.'):
    if f.endswith('.py'):
      mods.append(f.replace(".py", ""))
  return render("src-index", modules=mods)

@app.route('/<module>')
@show_exceptions
def render_file(module):
  """Render the code for a Python module, which will be syntax-highlighted
  client-side.
  """
  try:
    obj = __import__(module)
    fn = obj.__file__.replace(".pyc", ".py")
  except (ImportError, SyntaxError):
    if os.path.exists(module.replace(".", "/") + ".py"):
      # If the module can't be imported due to an error, but it can be read
      # directly, use the on-disk copy.
      fn = module + ".py"
    elif '.' in module:
      return redirect_to('.'.join(module.split('.')[:-1]))
    else:
      raise  # render the error
  return render("src", title=module, source=open(fn).read())

mount_at = make_mount_at(app)
