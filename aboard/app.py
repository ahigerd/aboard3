"""Module containing the Bottle app shared by all of the ABoard modules."""

import bottle

app = bottle.Bottle()
