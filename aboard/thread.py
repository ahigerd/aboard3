"""Module containing all functionality rooted at /thread."""

import bottle

from aboard_util import *
from .app import app
import model

@app.route("/t-<urlkey>")
@app.route("/t-<urlkey>/")
@app.route("/t-<urlkey>/<page>")
@app.route("/thread/<urlkey>")
def thread_redirect(urlkey, page=1):
  """Redirect to the canonical version of a thread URL."""
  key = model.Thread.decode_urlkey(urlkey)
  thread = key.get()
  if thread:
    urlkey = thread.urlkey
  if int(page) > 1:
    return redirect_to("/thread/%s/%s" % (urlkey, int(page)))
  else:
    return redirect_to("/thread/%s/" % urlkey)


@app.route("/thread/<urlkey>/")
@app.route("/thread/<urlkey>/<page>")
@show_exceptions
def thread_view(urlkey, page=1):
  """Render a thread page, focused on the list of posts it contains."""
  key = model.Thread.decode_urlkey(urlkey)
  # Unlike with forums, it's faster for threads to fetch the parent first
  # and the posts afterward. (Is this because it's an ancestor query?)
  thread = key.get_async()
  posts = model.Thread.get_posts(key, page=int(page)-1)
  thread = thread.get_result()
  thread.forum.prefetch()
  if not thread:
    # TODO: pretty error page
    return "thread not found"
  controls = PageControls(
      thread.last_post.count,
      model.POSTS_PER_PAGE,
      page,
      bottle.request.query_string)
  return render_page(
      render("postlist",
             thread=thread,
             pager=controls,
             forumkey=long_to_urlkey(thread.forum.id()),
             posts=posts.get_result()))

