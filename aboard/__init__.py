"""This module contains the main Bottle app for the user-facing ABoard
forum interface.
"""

from .app import app
from .forum import mount_at
import aboard.post   # ordering is sensitive, post must come before thread
import aboard.thread

