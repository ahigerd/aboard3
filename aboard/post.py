"""Module containing all functionality rooted at /post.

Also contains the post editor for threads.
"""

from google.appengine.ext import ndb
import bottle

from aboard_util import *
from .app import app
from .validate import ValidationResult, validate_post
import model
import session


# This needs to come before def thread_view() because otherwise the thread's
# page-navigation route takes precedence over this one.
@app.route("/thread/<threadkey>/post", method=('GET', 'POST'))
@app.route("/post/<postkey>/edit", method=('GET', 'POST'))
@ndb.toplevel
@show_exceptions
def post_editor(threadkey=None, postkey=None):
  """Render the post editor for replying to a thread or for editing an
  existing post.
  """
  # TODO: permissions
  # TODO: error handling
  if threadkey:
    thread = model.Thread.urlkey_get(threadkey)
    # TODO: error message if thread is None
    user = session.current_session().user
    post = model.Post(parent=thread.key, poster=user)
  else:
    post = model.Post.urlkey_get(postkey)
    thread = post.key.parent().get()
  first_post = (model.Post
                .query(ancestor=thread.key)
                .order(model.Post.timestamp)
                .get(keys_only=True)) == post.key
  errors = ValidationResult()
  if bottle.request.POST:
    post.body = bottle.request.POST['body']
    post.use_bbcode = 'use_bbcode' in bottle.request.POST
    post.use_signature = 'use_signature' in bottle.request.POST
    errors = validate_post(post, first_post and bottle.request.POST['title'])
    if errors.ok and 'post' in bottle.request.POST:
      if threadkey is not None:
        # Update the user's post count if this isn't an edit
        user.entity.update_post_count(1)
      if first_post:
        thread.update_first_post(bottle.request.POST['title'], post)
      else:
        ndb.transaction(post.put, xg=True)
      return redirect_to('/post/' + post.urlkey)
  return render_page(
      render("posteditor",
             editing=(threadkey is None),
             new_thread=False,
             no_title=errors.no_title,
             no_body=errors.no_body,
             first_post=first_post,
             post=post,
             thread=thread))


@app.route("/p-<urlkey>")
@app.route("/post/<urlkey>")
@show_exceptions
def post_view(urlkey):
  """Redirect to a specific post."""
  key = model.Post.decode_urlkey(urlkey)
  thread = key.parent().get_async()
  thread_posts = model.Post.query(ancestor=key.parent()).order(model.Post.timestamp)
  post_count = 0
  for postkey in thread_posts.iter(keys_only=True):
    if postkey == key:
      pass
    post_count += 1
  page = post_count / model.POSTS_PER_PAGE
  thread = thread.get_result()
  if page == 0:
    return redirect_to('/thread/%s#%s' % (thread.urlkey, urlkey))
  return redirect_to('/thread/%s/%s#%s' % (thread.urlkey, page + 1, urlkey))
