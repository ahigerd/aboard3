"""Module containing all functionality rooted at /forum.

Also contains the top-level index page.
"""

from google.appengine.ext import ndb
import bottle

from aboard_util import *
from .app import app
from .validate import ValidationResult, validate_post
import model
import session


@app.route("/")
@show_exceptions
def forum_index():
  """Render the main forum index page."""
  allforums = model.Forum.get_forum_list()
  return render_page(
      render("forumlist", categories=allforums))

@app.route('/forum/<urlkey>/newthread', method=('GET', 'POST'))
@ndb.toplevel
@show_exceptions
def new_thread(urlkey):
  """Render the post editor for creating a new thread and its first post."""
  forum = model.Forum.urlkey_get(urlkey)
  thread = model.Thread(title='', forum=forum.key)
  post = model.Post()
  errors = ValidationResult()
  if bottle.request.POST:
    user = session.current_session().user
    thread.poster = user
    thread.title = bottle.request.POST['title']
    post.poster = user
    post.body = bottle.request.POST['body']
    post.use_bbcode = 'use_bbcode' in bottle.request.POST
    post.use_signature = 'use_signature' in bottle.request.POST
    errors = validate_post(post, thread.title)
    if errors.ok and 'post' in bottle.request.POST:
      user.entity.update_post_count(1)
      @ndb.transactional(xg=True)
      def transaction(): # pylint: disable=missing-docstring
        threadkey = thread.put()
        # TODO: error message if thread insert fails
        post.key = ndb.Key(model.Post, None, parent=threadkey)
        post.put()
      transaction()
      return redirect_to('/thread/' + thread.urlkey)
  return render_page(
      render("posteditor",
             editing=False,
             new_thread=True,
             no_title=errors.no_title,
             no_body=errors.no_body,
             post=post,
             thread=thread,
             forum=forum))


@app.route('/forum/<urlkey>')
def forum_redirect(urlkey):
  """Redirect to the canonical version of a forum URL."""
  return redirect_to('/forum/%s/' % urlkey)


@app.route('/forum/<urlkey>/')
@app.route('/forum/<urlkey>/<page>')
@show_exceptions
def forum_view(urlkey, page=1):
  """Render a forum page, focused on the list of threads it contains."""
  key = model.Forum.decode_urlkey(urlkey)
  result = model.Forum.get_threads(key, int(page)-1)
  forum = key.get()
  if not forum:
    # TODO: pretty error page
    return "forum not found"
  threads, count = result.get_result()
  controls = PageControls(
      count,
      model.THREADS_PER_PAGE,
      page,
      bottle.request.query_string)
  return render_page(
      render("threadlist",
             pager=controls,
             forum=forum,
             threads=threads))


mount_at = make_mount_at(app)
