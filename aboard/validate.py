"""Module containing validation functions for ABoard."""

class ValidationResult(object):
  """The result of a post/thread validation. A default-constructed object
  has no errors set (ok returns True) and is suitable for use in a context
  where a validation result is required but no validation has been
  performed.

  no_title means that a title was required and was not provided.
  no_body means that the post was missing a body.
  """
  no_title = False
  no_body = False

  @property
  def ok(self):
    """Return True if none of this object's error flags are set."""
    return not any(self.__dict__.values())


def validate_post(post, title=False):
  """Validate a post and return a ValidationResult describing it.

  If title is False (not None) then it is ignored, otherwise it is required.
  """
  result = ValidationResult()
  result.no_title = title is not False and not title
  result.no_body = not post.body
  return result
