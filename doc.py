"""Documentation-related functionality centered around manipulating
docstrings and rendering HTML doc pages.

This module contains a Bottle app that will return pydoc HTML pages for
any requested module it can import.
"""

import pydoc
import sys

from aboard_util import *
import bottle

app = bottle.Bottle()

def trim(docstring):
  """Remove insignificant indentation from a docstring.

  This implementation is taken directly from PEP-0257.
  """
  if not docstring:
    return ''
  # Convert tabs to spaces (following the normal Python rules)
  # and split into a list of lines:
  lines = docstring.expandtabs().splitlines()
  # Determine minimum indentation (first line doesn't count):
  indent = sys.maxint
  for line in lines[1:]:
    stripped = line.lstrip()
    if stripped:
      indent = min(indent, len(line) - len(stripped))
  # Remove indentation (first line is special):
  trimmed = [lines[0].strip()]
  if indent < sys.maxint:
    for line in lines[1:]:
      trimmed.append(line[indent:].rstrip())
  # Strip off trailing and leading blank lines:
  while trimmed and not trimmed[-1]:
    trimmed.pop()
  while trimmed and not trimmed[0]:
    trimmed.pop(0)
  # Return a single string:
  return '\n'.join(trimmed)

def add_doc(obj, doc):
  """Adds a docstring to an object, preserving the original docstring as
  a suffix if it exists.
  """
  existing = trim(obj.__doc__)
  added = trim(doc)
  if existing:
    obj.__doc__ = added + '\n\n' + existing
  else:
    obj.__doc__ = added
  return obj

def set_doc(obj, doc):
  """Sets the docstring of an object, discarding any existing docstring.
  """
  obj.__doc__ = doc
  return obj

def add_doc_to_headline(obj, doc):
  """Adds a docstring to an object, preserving the first line of the
  original docstring as a suffix if it exists.
  """
  existing = obj.__doc__.split('\n')
  headline = ''
  for line in existing:
    headline = line.strip()
    if headline:
      break
  if headline:
    headline = '\n\n' + headline
  obj.__doc__ = trim(doc) + headline
  return obj

@app.route('/')
@app.route('/<module>.html')
@show_exceptions
def render_documentation(module="index"):
  """Render the documentation for the specified module as an HTML page.

  The module will be imported if necessary.
  """
  if module == "index":
    return pydoc.html.index(".")
  try:
    keys = module.split('.')
    obj = globals()[keys.pop(0)]
    while keys:
      obj = getattr(obj, keys.pop(0))
  except KeyError:
    obj = __import__(module)
    keys = module.split('.')[1:]
    while keys:
      obj = getattr(obj, keys.pop(0))
  return pydoc.html.document(obj)

mount_at = make_mount_at(app)
