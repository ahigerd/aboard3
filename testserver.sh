#!/bin/bash
LOGLEVEL="info"
PORT=9999
ADMIN_PORT=8000
HOST=0.0.0.0
EXTRA_ARGS=

while getopts "p:l:h:a:vxX:" opt; do
  case $opt in
    p)
      PORT=$OPTARG
      ;;
    a)
      ADMIN_PORT=$OPTARG
      ;;
    h)
      HOST=$OPTARG
      ;;
    l)
      LOGLEVEL=$OPTARG
      ;;
    v)
      LOGLEVEL=debug
      ;;
    x)
      EXTRA_ARGS="$EXTRA_ARGS --clear_datastore"
      ;;
    X)
      EXTRA_ARGS="$EXTRA_ARGS $OPTARG"
      ;;
    \?)
      exit 1;
      ;;
  esac
done

GAE_PATH=$(dirname `which dev_appserver.py`)

rm templates/tmpl_*.py
PYTHONPATH=$GAE_PATH/lib/jinja2-2.6 python dev.py &
REBUILDER=$!
dev_appserver.py --port=$PORT --host=$HOST --admin_port=$ADMIN_PORT --admin_host=$HOST --datastore_path=../appengine-store.db ./app.yaml --dev_appserver_log_level=$LOGLEVEL --log_level=$LOGLEVEL $EXTRA_ARGS
kill $REBUILDER
