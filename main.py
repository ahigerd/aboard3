"""The main bootstrap module for a demonstration ABoard site.

Deployments should customize this file as appropriate for their needs.
"""

import aboard
import doc
import session
import src
import testing
import user

app = aboard.app
doc.mount_at(app, '/doc')
testing.mount_at(app, '/testing')
src.mount_at(app, '/src')
aboard.mount_at(app, '/loopback')
session.mount_in(app)
user.mount_at(app, '/user')
