"""This module contains the user-facing UI for user profiles and settings
management.
"""

import copy

from aboard_util import *
import bottle
import model
import session


app = bottle.Bottle()


@app.route('/', method=('GET', 'POST'))
@show_exceptions
def user_cp():
  """Provide an interface for the user to modify profile settings."""
  if not session.current_session().logged_in:
    # TODO: going to need a service locator to resolve this if it's not
    # mounted at the default location
    return redirect_to('/login', absolute=True)
  user = copy.copy(session.current_session().user.entity)
  success = False
  password_changed = False
  password_mismatch = False
  password_error = False
  if bottle.request.POST:
    user.title = bottle.request.POST['title'].strip()
    if not user.title:
      user.title = None
    user.signature_raw = bottle.request.POST['signature'].strip()
    user.signature_use_bbcode = 'use_bbcode' in bottle.request.POST
    password = bottle.request.POST['newpassword'].strip()
    if password:
      current_password = bottle.request.POST['password'].strip()
      confirm = bottle.request.POST['confirm'].strip()
      if password != confirm:
        password_mismatch = True
      if not user.check_password(current_password):
        password_error = True
      if not password_mismatch and not password_error:
        user.set_password(password)
        password_changed = True
    if not password_mismatch and not password_error:
      user.put()
      success = True
  if not user.title:
    user.title = ''
  return render_page(
      render("editprofile",
             user=user,
             success=success,
             password_changed=password_changed,
             password_mismatch=password_mismatch,
             password_error=password_error))


@app.route('/list')
def member_list_redirect():
  """Redirect to the member list."""
  return redirect_to('/list/')



_member_sort = {
    'n': model.User.username,
    't': model.User.title,
    'pc': model.User.post_count,
    'j': model.User.joined,
}


@app.route('/list/')
@app.route('/list/<page>')
@show_exceptions
def member_list(page=None): # pylint: disable=unused-argument
  """Display the selected page of the member list, sorted based on the GET
  variable 'sort'.
  """
  user_count = model.User.query().count_async()
  sort = bottle.request.GET.get('sort', 'n-a').split('-')
  sortfield = _member_sort.get(sort[0], _member_sort['n'])
  reverse = len(sort) > 1 and sort[1] == 'd'
  if reverse:
    sortfield = -sortfield

  controls = PageControls(user_count.get_result(),
                          model.THREADS_PER_PAGE,
                          page,
                          bottle.request.query_string)
  keys = model.User.query().order(sortfield).fetch_async(
      offset=controls.pagestart,
      limit=model.THREADS_PER_PAGE,
      keys_only=True)
  return render_page(
      render("memberlist",
             sort=SortControls('', sort[0], reverse),
             pager=controls,
             users=model.User.query(model.User.key.IN(keys.get_result())).iter()))


@app.route('/<urlkey>')
@show_exceptions
def profile_page(urlkey):
  """Display the profile for a single user."""
  user = model.User.urlkey_get(urlkey)
  if not user:
    # TODO: pretty error message
    return "user not found"
  return render_page(
      render("profile",
             user=user,
             post_count=user.post_count))


mount_at = make_mount_at(app)
