"""Constants shared across ABoard data models."""

THREADS_PER_PAGE = 30
POSTS_PER_PAGE = 20
