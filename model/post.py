"""Models for posts and some of their metadata."""

from __future__ import absolute_import

from google.appengine.ext import ndb

from doc import add_doc, add_doc_to_headline
import aboard_util
from .prefetching_key import PrefetchingKeyProperty
import model


@aboard_util.urlkeys(None)
class Post(ndb.Model):
  """Models a single post in a thread."""
  poster = add_doc_to_headline(
      PrefetchingKeyProperty(kind=model.user.User),
      """The user that created the post.""")
  timestamp = add_doc(
      ndb.DateTimeProperty(auto_now_add=True),
      """The timestamp when the post was first created.""")
  edit_timestamp = add_doc(
      ndb.DateTimeProperty(auto_now=True),
      """The timestamp when the post was last edited.""")
  html = add_doc_to_headline(
      ndb.ComputedProperty(
          lambda self: aboard_util.format_text(self.body, self.use_bbcode),
          indexed=False),
      """The HTML-formatted post content, suitable for rendering.""")
  body = add_doc(
      ndb.TextProperty(indexed=False, compressed=True, default=''),
      """The raw post content, suitable for editing.""")
  use_bbcode = add_doc(
      ndb.BooleanProperty(indexed=False, default=True),
      """Whether or not to convert bbcode to HTML.""")
  use_signature = add_doc(
      ndb.BooleanProperty(indexed=False, default=True),
      """Whether or not to display the user's signature on the post.""")
  _put_hook_thread = False

  @property
  def urlkey(self):
    """Generate a urlkey for this post."""
    return aboard_util.long_to_urlkey(self.key.parent().id() << 64 | self.key.id())

  @classmethod
  def decode_urlkey(cls, urlkey):
    """Separate a post urlkey into its thread and post components, returning
    a proper ndb.Key object.
    """
    keybits = aboard_util.urlkey_to_long(urlkey)
    if not keybits:
      return None
    return ndb.Key(model.thread.Thread, keybits >> 64,
                   cls, keybits & 0xFFFFFFFFFFFFFFFF)

  def _pre_put_hook(self):
    """Load the thread and forum in preparation for updating its last post
    information, if this is a new post.

    Note that the caller is responsible for invoking User.update_post_count
    when creating or deleting a post.
    """
    if self.has_complete_key():
      self._put_hook_thread = False
      return
    self._put_hook_thread = self.key.parent().get_async()

  def _post_put_hook(self, future): # pylint: disable=unused-argument
    """Update the last post information of the thread and forum if this is a
    new post.
    """
    # TODO: This is probably where the search API hook needs to go.
    if not self._put_hook_thread:
      return
    parent_thread = self._put_hook_thread.get_result()
    put_hook_forum = parent_thread.forum.get_async()
    parent_thread.last_post.count += 1
    parent_thread.last_post.poster = self.poster
    parent_thread.last_post.timestamp = self.timestamp
    parent_thread.put_async()
    forum = put_hook_forum.get_result()
    forum.last_post.count += 1
    forum.last_post.poster = self.poster
    forum.last_post.title = parent_thread.title
    forum.last_post.timestamp = self.timestamp
    forum.put_async()


class PostingHistory(ndb.Model):
  """Models the metadata parameters of the last post in a thread or
  forum.
  """
  poster = add_doc(
      PrefetchingKeyProperty(kind=model.user.User),
      """The last user to post in the thread or forum.""")
  timestamp = add_doc(
      ndb.DateTimeProperty(),
      """The last time a new entry was posted inside the thread or forum.""")
  title = add_doc(
      ndb.StringProperty(),
      """For forums only, the title of the last thread to be modified.""")
  count = add_doc(
      ndb.IntegerProperty(default=0),
      """For forums, the number of threads. For threads, the number of posts.""")
