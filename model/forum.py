"""Models for forums and their groupings."""

from __future__ import absolute_import

from google.appengine.ext import ndb

from doc import add_doc, add_doc_to_headline
import aboard_util
from .post import PostingHistory
from .constants import *
from .prefetching_key import PrefetchingKeyProperty
import model


class Category(ndb.Model):
  """Models a category containing forums.

  Because categories are expected to be updated very infrequently, all of
  the properties are indexed to enable reading the category list with a
  projection query.

  You should probably not query for categories directly outside of the
  category administration tool; use Forum.get_forum_list for normal use.
  """
  title = add_doc(
      ndb.StringProperty(),
      """The title to be displayed for the category.""")
  icon = add_doc(
      ndb.StringProperty(),
      """The URL to the icon to be displayed for the category.""")
  sequence = add_doc(
      ndb.IntegerProperty(),
      """The position of the category in the board index.""")

  index_projection = [title, icon, sequence]


@aboard_util.urlkeys('title')
class Forum(ndb.Model):
  """Models a forum."""
  category = add_doc(
      PrefetchingKeyProperty(kind=Category),
      """The category that contains the forum.""")
  title = add_doc(
      ndb.StringProperty(),
      """The displayed title of the forum.""")
  last_post = add_doc_to_headline(
      ndb.LocalStructuredProperty(PostingHistory, default=PostingHistory()),
      """A PostingHistory object containing information about the last
      post made in the forum.
      """)
  sequence = add_doc(
      ndb.IntegerProperty(),
      """The position of the forum in its category.""")

  @classmethod
  def get_forum_list(cls):
    """Retrieve all of the forums on the board, sorted into categories."""
    result = []
    categories = {}
    forums = Forum.query().order(cls.sequence).fetch_async()
    for category in Category.query(projection=Category.index_projection).order(Category.sequence):
      category.forums = []
      result.append(category)
      categories[category.key] = category
    for forum in forums.get_result():
      categories[forum.category].forums.append(forum)
    return result

  @staticmethod
  @ndb.tasklet
  def get_threads(key, page=0, pagesize=THREADS_PER_PAGE, prefetch_users=True):
    """Retrieve one page of threads in the forum asynchronously, returning a
    Future object that will contain the list of threads and the total number
    of threads in the forum."""
    query = (model.thread.Thread
             .query(model.thread.Thread.forum == key,
                    projection=model.thread.Thread.forum_projection)
             .order(-model.thread.Thread.last_post.timestamp))
    count = yield query.count_async()
    result = yield query.fetch_async(pagesize, offset=pagesize*page)
    if prefetch_users:
      PrefetchingKeyProperty.prefetch_sequence(result, ('poster', 'last_post.poster'))
    raise ndb.Return((result, count))
