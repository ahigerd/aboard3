"""Class for user data and their groups."""

from __future__ import absolute_import

import hashlib
import os

from google.appengine.ext import ndb

from doc import add_doc
import aboard_util
import model


@aboard_util.urlkeys('username')
class User(ndb.Model):
  """Models a single user."""
  username = add_doc(
      ndb.StringProperty(),
      """The user's username, used for presentation and logging in.

      As this can be modified after creation, do not use the username for a
      foreign key. Use User.key instead.
      """)
  joined = add_doc(
      ndb.DateTimeProperty(auto_now_add=True),
      """Timestamp when the user registered.""")
  title = add_doc(
      ndb.StringProperty(),
      """The user's title, typically displayed below the username on posts.""")
  post_count = add_doc(
      ndb.IntegerProperty(default=0),
      """The user's post count, cached for queryability.""")
  password_hash = add_doc(
      ndb.BlobProperty(indexed=False),
      """The user's password, securely hashed with the user key and a
      randomly-generated salt.
      """)
  password_salt = add_doc(
      ndb.BlobProperty(indexed=False),
      """The randomly-generated salt used in the password hash.""")
  signature = add_doc(
      ndb.TextProperty(indexed=False),
      """The user's signature block, HTML-formatted.""")
  signature = add_doc(
      ndb.ComputedProperty(
          lambda self: aboard_util.format_text(self.signature_raw, self.signature_use_bbcode),
          indexed=False),
      """The user's HTML-formatted signature block, suitable for rendering.""")
  signature_raw = add_doc(
      ndb.TextProperty(indexed=False, compressed=True, default=''),
      """The user's raw signature content, suitable for editing.""")
  signature_use_bbcode = add_doc(
      ndb.BooleanProperty(indexed=False, default=True),
      """Whether or not to convert bbcode to HTML in the user's signature.""")
  email = add_doc(
      ndb.StringProperty(),
      """The user's e-mail address.""")

  def set_password(self, password):
    """Set the user's password using a newly-generated salt.

    Note that this function will fail on a user that has not yet been saved
    to the database.
    """
    self.password_salt = os.urandom(16)
    self.password_hash = self.hash_password(password)

  def hash_password(self, password):
    """Hash a plaintext password with the user's key and salt."""
    salted = self.key.urlsafe() + password + self.password_salt
    return hashlib.sha256(salted).digest()

  def check_password(self, attempt):
    """Validate that the provided password matches the user's stored
    password."""
    return self.hash_password(attempt) == self.password_hash

  @ndb.tasklet
  def update_post_count(self, adjustment=0):
    """Asynchronously update the user's post count, optionally modifying it
    to represent a pending uncommitted post, returning an ndb.Future to
    allow waiting for completion.

    This function is intended to be used opportunistically, outside of a
    transaction for efficiency's sake, before the post is inserted. The post
    count could, therefore, be off by one in rare cases where the post
    transaction fails after the post count was updated.
    """
    new_count = yield model.post.Post.query(model.post.Post.poster == self.key).count_async()
    self.post_count = new_count + adjustment
    yield self.put_async()
    raise ndb.Return()

  def _post_put_hook(self, future): # pylint: disable=unused-argument
    if 'session' not in globals():
      global session
      import session
    if self.key == session.current_session().user:
      session.current_session().user.prefetch()

