"""Class for threads."""

from __future__ import absolute_import

from google.appengine.ext import ndb

from doc import add_doc
from model import user
from .constants import *
from .forum import Forum
from .post import Post, PostingHistory
from .prefetching_key import PrefetchingKeyProperty
import aboard_util


@aboard_util.urlkeys('title')
class Thread(ndb.Model):
  """Models a thread."""
  forum = add_doc(
      PrefetchingKeyProperty(kind=Forum, projection=[Forum.title]),
      """The forum that contains the thread.""")
  poster = add_doc(
      PrefetchingKeyProperty(kind=user.User),
      """The user that created the thread.""")
  title = add_doc(
      ndb.StringProperty(),
      """A textual title to be displayed for the thread.""")
  last_post = add_doc(
      ndb.StructuredProperty(PostingHistory, default=PostingHistory()),
      """A PostingHistory object containing information about the last
      post made in the thread.
      """)
  locked = add_doc(
      ndb.BooleanProperty(default=False),
      """Whether to forbid new posts in the thread.""")


  #: The fields necessary to render the list of threads in a forum.
  forum_projection = [
      'poster',
      'title',
      'last_post.count',
      'last_post.poster',
      'last_post.timestamp'
  ]

  @staticmethod
  @ndb.tasklet
  def get_posts(key, page=0, pagesize=POSTS_PER_PAGE, prefetch_users=True):
    """Retrieve one page of posts in the thread asynchronously, returning a
    Future object that will contain the list."""
    post_keys = yield (Post
                       .query(ancestor=key)
                       .order(Post.timestamp)
                       .fetch_async(limit=pagesize,
                                    keys_only=True,
                                    offset=page*pagesize))
    if len(post_keys) == 0:
      raise ndb.Return([])
    result = yield (Post
                    .query(Post.key.IN(post_keys), ancestor=key)
                    .fetch_async(batch_size=len(post_keys)))
    if prefetch_users:
      PrefetchingKeyProperty.prefetch_sequence(result, 'poster')
    raise ndb.Return(result)

  @ndb.transactional(xg=True)
  def update_first_post(self, title, post):
    """Update the first post of the thread as well as the thread's title,
    as a single transaction.
    """
    if self.title == title:
      # Don't waste writes if it's already correct
      return
    assert post.key.parent() == self.key
    # Also see if we need to update the forum's metadata
    firstThreadKey = (Thread
                      .query(Thread.forum == self.forum, keys_only=True)
                      .order(-Thread.last_post.timestamp)
                      .get_async())
    self.title = title
    self.put_async()
    post.put_async()
    if firstThreadKey.get_result() == self.key:
      forum = self.forum.get_full()
      forum.last_post.title = title
      forum.put_async()

  @ndb.transactional(xg=True)
  def move_to_forum(self, destination):
    """Relocate a thread into another forum, updating the relevant metadata
    in the process.
    """
    if self.forum == destination:
      # Don't waste writes if it's already correct
      return
    # Fetch non-projected records for the old and new forums
    oldForum = self.forum.get_full_async()
    if destination.get_full_async:
      newForum = destination.get_full_async()
    else:
      newForum = destination.get_async()
    # async queue: oldForum, newForum

    # Get the first two threads out of the old forum and the first thread out of the new forum
    oldForumFirstThreadKeys = (Thread
                               .query(Thread.forum == self.forum, keys_only=True)
                               .order(-Thread.last_post.timestamp)
                               .fetch_async(2))
    newForumFirstThread = (Thread
                           .query(Thread.forum == destination,
                                  projection=[Thread.last_post.timestamp])
                           .order(-Thread.last_post.timestamp)
                           .get_async())
    # async queue: oldForum, newForum, oldForumFirstThreadKeys, newForumFirstThread

    # Update the thread entity
    self.forum = destination
    self.put_async()

    # Update the thread counts
    oldForum = oldForum.get_result()
    oldForum.last_post.count -= 1
    newForum = newForum.get_result()
    newForum.last_post.count += 1
    # async queue: oldForumFirstThreadKeys, newForumFirstThread

    # If the thread being moved was the first thread in the old forum, fetch the new first thread
    oldForumFirstThreadKeys = oldForumFirstThreadKeys.get_result()
    # async queue: newForumFirstThread
    oldForumNewFirstThread = None
    if len(oldForumFirstThreadKeys) > 0 and oldForumFirstThreadKeys[0] == self.key:
      if len(oldForumFirstThreadKeys) > 1:
        oldForumNewFirstThread = oldForumFirstThreadKeys[1].get_async()
    # async queue: newForumFirstThread, oldForumNewFirstThread

    # If the first thread in the destination forum is older than the thread being moved, update the
    # destination forum metadata
    newForumFirstThread = newForumFirstThread.get_result()
    # async queue: oldForumNewFirstThread
    if newForumFirstThread and newForumFirstThread.last_post.timestamp < self.last_post.timestamp:
      newForum.last_post.poster = self.last_post.poster
      newForum.last_post.timestamp = self.last_post.timestamp
      newForum.last_post.title = self.title
    newForum.put_async()

    # If we need to update the old forum, do so
    if oldForumNewFirstThread:
      oldForumNewFirstThread = oldForumNewFirstThread.get_result()
      if oldForumNewFirstThread:
        oldForum.last_post.poster = oldForumNewFirstThread.last_post.poster
        oldForum.last_post.timestamp = oldForumNewFirstThread.last_post.timestamp
        oldForum.last_post.title = oldForumNewFirstThread.title
      else:
        oldForum.last_post.poster = None
        oldForum.last_post.timestamp = None
        oldForum.last_post.title = None
    oldForum.put_async()
    # async queue: empty
