"""Classes for keys that automatically fetch their referents."""

from google.appengine.ext import ndb

import model

#: A singleton to represent an entity that has not been queried for at
# all, as opposed to a failed query or a boolean False value.
_NotFetched = object()

# pylint: disable=attribute-defined-outside-init
class PrefetchingKey(object):
  """Data descriptor / proxy class that wraps an ndb.Key with transparent
  cached asynchronous fetch functionality.
  """
  def __init__(self, key=None, entity=_NotFetched, kind=None, projection=None):
    if entity and entity is not _NotFetched:
      if not key:
        key = entity.key
      else:
        assert key == entity.key, "entity.key does not match key"
    if not kind:
      if key.kind():
        kind = key.kind()
    else:
      assert key.kind() == kind, "key is not of the required kind"

    object.__setattr__(self, '_key', key)
    object.__setattr__(self, '_entity', entity)
    object.__setattr__(self, '_kind', kind)
    object.__setattr__(self, '_projection', projection)

  @property
  def entity(self):
    """Return the entity associated with the key, blocking until the value
    is available.
    """
    return self.get()

  def get(self):
    """Return the entity associated with the key. Synonym for entity, for
    API compatibility with normal ndb.Key objects.
    """
    if not self._key or self._key.id() is None:
      # This key couldn't possibly return anything.
      return None
    elif isinstance(self._entity, ndb.Future):
      # There's already been a query, so block on the result.
      self._entity = self._entity.get_result()
    elif self._entity is _NotFetched:
      # The entity hasn't been prefetched, so query for it.
      if self._projection:
        kind = getattr(model, self._key.kind())
        self._entity = kind.query(kind.key == self._key,
                                  projection=self._projection).get()
      else:
        self._entity = self._key.get()
    return self._entity

  def get_full(self):
    """Synchronously retrieve the complete entity, even if the key was
    originally created to fetch a projection.

    The entity is queried independently each time this function is called.
    The value of the entity property remains unchanged.
    """
    return self._key.get()

  def get_full_async(self):
    """Asynchronously query for the complete entity from the database, even
    if the key was originally created to fetch a projection.

    The entity is queried independently each time this function is called.
    The value of the entity property remains unchanged.
    """
    return self._key.get_async()

  def prefetch(self):
    """Asynchronously query for the associated entity, to be later
    retrieved using the .entity property.

    This function can also be used to force the entity to be reloaded from
    the database.
    """
    if self._projection:
      kind = getattr(model, self._key.kind())
      self._entity = kind.query(kind.key.IN([self._key]),
                                projection=self._projection).get_async()
    else:
      self._entity = self._key.get_async()

  def __repr__(self):
    return 'Prefetching' + repr(self._key)
  __repr__.__doc__ = ndb.Key.__repr__.__doc__

  # Copy metamethods from ndb.Key, because __getattribute__/__getattr__ forwarding doesn't work
  # on metamethods.
  __hash__ = ndb.Key.__dict__['__hash__']
  __getstate__ = ndb.Key.__dict__['__getstate__']
  __setstate__ = ndb.Key.__dict__['__setstate__']
  for name in ('__eq__', '__ne__', '__lt__', '__le__', '__gt__', '__ge__'):
    # pylint: disable=no-self-argument,protected-access
    def _generate_metamethod(name):
      """Make a metamethod that operates on the proxied key."""
      method = ndb.Key.__dict__[name]
      return lambda self, other: method(self._key, getattr(other, '_key', other))
    locals()[name] = _generate_metamethod(name)

  def __getattr__(self, name):
    """Forward attribute requests to the key."""
    # Only called if lookup fails, so e.g. _key still works
    return getattr(self._key, name)

  def __setattr__(self, name, value):
    """Forward attribute assignments to the key, except for those that
    must be handled by the proxy.
    """
    if name == 'entity':
      raise AttributeError(name + ' property is read-only')
    if name in self.__dict__:
      self.__dict__[name] = value
    else:
      setattr(self._key, name, value)

  def __delattr__(self, name):
    """Forward attribute deletions to the key."""
    delattr(self._key, name)


# pylint: disable=protected-access
class PrefetchingKeyProperty(ndb.KeyProperty):
  """Wraps an ndb.KeyProperty to offer a caching, asynchronous interface to
  read the entity referred to by the key.

  In addition to the normal attributes that a Key has, the object returned
  by a PrefetchingKeyProperty has an .entity property that contains or
  transparently (and synchronously) queries for the referred entity and a
  .prefetch() method that asynchronously fetches it to be subsequently read
  using the .entity property.
  """
  def __init__(self, *args, **kwargs):
    if 'projection' in kwargs:
      self._projection = kwargs['projection']
      del kwargs['projection']
    else:
      self._projection = None
    super(PrefetchingKeyProperty, self).__init__(*args, **kwargs)

  def _from_base_type(self, value):
    """Convert a Key to a PrefetchingKey."""
    if isinstance(value, PrefetchingKey):
      return value
    return PrefetchingKey(value, kind=self._kind, projection=self._projection)

  def _to_base_type(self, value):
    """Convert a PrefetchingKey to a Key."""
    if isinstance(value, PrefetchingKey):
      return value._key
    return value

  def _validate(self, value):
    """Make sure the entity kind of an assigned value matches the required
    kind specified in the property.

    If assigned an ndb.Key, coerce it to a PrefetchingKey.
    """
    if self._kind and value.kind() != self._kind:
      raise ValueError("Cannot assign key of type '" + value.kind() +
                       "' to property of type '" + self._kind + "'")
    return self._from_base_type(value)

  @staticmethod
  def resolveattr(obj, field, *args):
    """Look up a nested attribute using dot notation.

    If a second parameter is passed, set the attribute to that value.
    """
    path = field.split('.')
    target = None
    if len(args) > 0:
      target = path[-1]
      path = path[:-1]
    try:
      for attr in path:
        obj = getattr(obj, attr)
    except AttributeError:
      return None
    if target:
      setattr(obj, target, args[0])
      return args[0]
    else:
      return obj

  @classmethod
  def prefetch_sequence(cls, seq, fields):
    """Iterate over a sequence of entities, prefetching the entities for
    named attribute(s).

    If fields is a string, it is used as the name of the attribute.
    Otherwise, fields is treated as a sequence, and each element is used as
    the name of an attribute.
    """
    if isinstance(fields, basestring):
      fields = (fields,)
    gets = {}
    for entity in seq:
      for field in fields:
        keyfield = cls.resolveattr(entity, field)
        if keyfield is None:
          continue
        if not isinstance(keyfield, PrefetchingKey):
          if isinstance(keyfield, ndb.Key):
            keyfield = cls.resolveattr(entity, field, cls._from_base_value(keyfield))
          else:
            raise TypeError("Property '" + field + "' of entity is not a Key")
        # If this key is already going to be fetched, add the destination
        # field to the appropriate list in the gets table, otherwise add a
        # new list to it.
        if keyfield._key in gets:
          gets[keyfield._key].append(keyfield)
        else:
          gets[keyfield._key] = [keyfield]

    keys = gets.keys()
    futures = ndb.get_multi_async(keys)
    for key, future in zip(keys, futures):
      for keyfield in gets[key]:
        keyfield._entity = future
