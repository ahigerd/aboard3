"""Data model for user sessions."""

import datetime

from google.appengine.ext import ndb

from doc import add_doc
from .user import User
from .prefetching_key import PrefetchingKeyProperty


class Session(ndb.Model):
  """Models a session, whether logged in or not."""
  user = add_doc(
      PrefetchingKeyProperty(kind=User),
      """The logged-in user for the session, if any.""")
  # This might need to be indexed if we decide we want to proactively query for idle sessions, but
  # we should really strive to keep this entity as cheap as possible because it's going to see a lot
  # of churn.
  last_update = add_doc(
      ndb.DateTimeProperty(indexed=False, auto_now=True),
      """The last time the session was updated. Don't update it too often.""")

  @property
  def logged_in(self):
    """Return whether or not the session has a logged-in user associated
    with it.
    """
    return (self.user is not None and
            self.last_update < datetime.datetime.now() + datetime.timedelta(days=7))

  def __setattr__(self, key, value):
    """Flag the session as being dirty if its properties are modified."""
    if key != 'dirty':
      object.__setattr__(self, 'dirty', True)
    object.__setattr__(self, key, value)

  def _post_put_hook(self, future): # pylint: disable=unused-argument
    """Flag the session as clean after it has been written to persistent
    storage.
    """
    object.__setattr__(self, 'dirty', False)
