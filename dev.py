"""Tools only for use in development."""

# pylint: disable=wrong-import-position
# pylint: disable=wrong-import-order

DEV_LOADER = True

import vendor
vendor.add("lib")

import os
import re
import string
import sys
import time

import jinja2

import aboard_util

import watchdog.events
import watchdog.observers

template_env = jinja2.Environment(loader=jinja2.FileSystemLoader("template_src/"),
                                  trim_blocks=True)
aboard_util.install_filters(template_env)


def rebuild_templates():
  """Rebuild all templates using Jinja2."""
  template_env.compile_templates("templates/",
                                 zip=None,
                                 log_function=lambda x: sys.stdout.write(x + '\n'),
                                 extensions=["html"])


def rebuild_template(name):
  """Rebuild a single template using Jinja2."""
  name = name.rpartition('/')[2]
  template_env.compile_templates("templates/",
                                 zip=None,
                                 log_function=lambda x: sys.stdout.write(x + '\n'),
                                 filter_func=lambda fn: fn == name)


def build_css_file(source):
  """Preprocess a CSS file.

  Lines of the form "name = text" define macros that will be substituted
  when $name or ${name} is encountered later in the file.

  Some very basic minification is performed by removing extraneous
  whitespace. No syntax checking is performed. The resulting file will be
  stored in the static/ subdirectory.
  """
  variables = {}
  content = []
  for line in open(source, 'r'):
    line = re.sub(r"\s+", " ", line.strip())
    line = re.sub(r"\s?([(){}>~#:;=,])\s?", r"\1", line)
    line = string.Template(line).safe_substitute(variables)
    if "=" in line:
      (head, _, tail) = line.partition("=")
      if ":" not in head and "[" not in head:
        variables[head] = tail
        continue
    content.append(line)
  with open('static/' + source.rpartition('/')[2], 'w') as f:
    f.write(''.join(content).replace(";}", "}"))


def rebuild_css_files():
  """Rebuilds all CSS files in the stylesheets directory."""
  for f in os.listdir("stylesheets"):
    if f.endswith(".css"):
      build_css_file("stylesheets/" + f)


class CssRebuilder(watchdog.events.FileSystemEventHandler):
  """Rebuilds CSS files when changes are detected."""
  def on_created(self, event):
    """Trigger rebuilding a CSS file due to a received event."""
    if not event.is_directory and event.src_path.endswith(".css"):
      print "Rebuilding " + event.src_path
      build_css_file(event.src_path)
  on_modified = on_created


class TemplateRebuilder(watchdog.events.FileSystemEventHandler):
  """Rebuilds template files when changes are detected."""
  def on_created(self, event):
    """Trigger rebuilding a template due to a received event."""
    if not event.is_directory and event.src_path.endswith(".html"):
      print "Rebuilding " + event.src_path
      rebuild_template(event.src_path)
  on_modified = on_created


if __name__ == "__main__":
  # When launched from the command line, use an Observer to automatically
  # rebuild modified CSS and template files.
  rebuild_css_files()
  rebuild_templates()
  observer = watchdog.observers.Observer()
  observer.schedule(CssRebuilder(), "stylesheets")
  observer.schedule(TemplateRebuilder(), "template_src")
  observer.start()
  try:
    while True:
      time.sleep(1)
  except BaseException:
    observer.stop()
  observer.join()
