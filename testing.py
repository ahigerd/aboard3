"""A collection of testing functions for exercising parts of the code."""

from aboard_util import *
from google.appengine.api import memcache
from google.appengine.ext import ndb
import bottle
import model
import session


app = bottle.Bottle()


@ndb.transactional(xg=True)
@ndb.toplevel
def populate():
  """Create some sample data for an initial test."""
  user = model.User(username="Coda")
  user.put()
  user.set_password("test")
  user.put()

  cat1 = model.Category(title="Category 2", sequence=1)
  cat1.put()
  cat2 = model.Category(title="Category 1", sequence=0)
  cat2.put()

  forum = model.Forum(category=cat1.key, title="Forum C", sequence=0)
  forum.put()
  forum = model.Forum(category=cat2.key, title="Forum B", sequence=1)
  forum.put()
  forum = model.Forum(category=cat2.key, title="Forum A", sequence=0)
  forum.put()

  thread = model.Thread(
      forum=forum.key, poster=user.key, title="Thread",
      last_post=model.PostingHistory(poster=user.key))
  thread.put()

  post = model.Post(parent=thread.key, poster=user.key, body="body")
  post.use_formatting = False
  post.put()

  post = model.Post(parent=thread.key, poster=user.key, body="body 2")
  post.use_formatting = False
  post.put()
  post.body = "body 2 (edited)"
  post.put()

  session.current_session().user = user.key
  session.current_session().put()


@app.route('/populate')
@show_exceptions
@ndb.toplevel
def populate_page():
  """Populate the database with testing data, if it hasn't been done before,
  and then redirect to the recalculation routine.
  """
  # I have no idea why this gets called twice, and nothing I can do seems to
  # solve it, so atomic operations to the rescue!
  if memcache.add("populate", 1):
    print "populating"
    populate()
  else:
    print "race: not populating"
  ctx = ndb.get_context()
  ctx.flush()
  # pylint: disable=protected-access
  return session._login_user(model.User.query().get(), '/recalc', False)


@app.route('/recalc')
@ndb.toplevel
def recalc_post_counts():
  """Recalculate the post count for all users in the database."""
  for user in model.User.query():
    user.update_post_count()
  return redirect_to('/', absolute=True)

mount_at = make_mount_at(app)
